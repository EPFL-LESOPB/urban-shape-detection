#!/bin/bash

## ====== Settings

#SBATCH --account=cadmos

## specific for GPU nodes (free account)
#SBATCH --partition=gpu --qos=gpu --gres=gpu:4

#SBATCH --nodes=1
#SBATCH --mem=16G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --time=12:00:00

#SBATCH --workdir ./logs
#SBATCH --mail-type=ALL
#SBATCH --mail-user=roberto.castello@epfl.ch

#SBATCH -o FCN_vassel_train.out # Standard output
#SBATCH -e FCN_vassel_train.err # Standard error

echo STARTING AT $(date)

## == Specify here the needed modules and the virtual env

module purge
module load gcc cuda cudnn python     
source $HOME/solar_deployment/1.9-gpu/tensorflow-1.9/bin/activate

## == Specify here the commands to execute 

## Run. Argument to pass (in order): data location, dataset name, query name, #estimators, # nodes, partition (CPU or GPU) 
python $HOME/solar_deployment/FCN_semantic_tensorflow/$1.py

source deactivate

echo FINISHED AT $(date)
