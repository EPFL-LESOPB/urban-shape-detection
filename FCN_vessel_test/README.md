# Fully convolutional neural network (FCN) for semantic segmentation with tensorflow

This is a simple implementation of a fully convolutional neural network (FCN). The net is based on fully convolutional neural net described in the paper [Fully Convolutional Networks for Semantic Segmentation](https://arxiv.org/pdf/1605.06211.pdf). 
The net produces pixel-wise annotation as a matrix in the size of the image with the value of each pixel corresponding to its class. 

## Test instructions and results

This network has been tested with Python 3.6 and Tensorflow 1.9. The training was done using 4 K40 NVIDIA cards on Deneb accelerated nodes. 
A pre-trained VGG16, downloaded from [here](https://drive.google.com/file/d/0B6njwynsu2hXZWcwX0FKTGJKRWs/view?usp=sharing), has been used and placed in the /Model_Zoo subfolder in the main code folder.
The net was tested on a dataset of annotated images of materials in glass vessels. This dataset can be downloaded from [here](https://drive.google.com/file/d/0B6njwynsu2hXRFpmY1pOV1A4SFE/view?usp=sharing) and should be placed in a new subfolder named /Data_Zoo.

Configuration of the following variables in train.py needs to be done:

- Folder name of the training images in Train_Image_Dir
- Folder name for the ground truth labels in Train_Label_DIR
- Number of classes/labels in NUM_CLASSES

The training took approximately 3 hours on GPU nodes (4000 iterations, 4 classes).

To run on SCITAS queues use the following script:

sbatch_tensorflow.sh \<step\>

where \<step\> can be *train* or *inference* according to the step to be run. To run the prediction of pixelwise annotation the file is inference.py
