import os
from osgeo import gdal

in_path = '/work/hyenergy/raw/SwissTopo/RGB_25cm/data_orig/'
input_filename = 'SI_25_2013_1091-14.tif'

out_path = '/work/hyenergy/raw/SwissTopo/RGB_25cm/data_resized/'
output_filename = 'tile_'

tile_size_x = 250
tile_size_y = 250

ds = gdal.Open(in_path + input_filename)
band = ds.GetRasterBand(1)
xsize = band.XSize
ysize = band.YSize

for i in range(0, xsize, tile_size_x):
    for j in range(0, ysize, tile_size_y):
        com_string = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", " + str(j) + ", " + str(tile_size_x) + ", " + str(tile_size_y) + " " + str(in_path) + str(input_filename) + " " + str(out_path) + str(output_filename) + str(i) + "_" + str(j) + ".tif"
        os.system(com_string)
